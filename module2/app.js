(function(){
    'use strict';
    angular.module("ShoppingListCheckOff", [])
    .controller("ToBuyController", ToBuyController)
    .controller("AlreadyBoughtController", AlreadyBoughtController)
    .service("ShoppingListCheckOffService", ShoppingListCheckOffService);
    ToBuyController.$inject = ["ShoppingListCheckOffService"]; // Protection from Injection
    function ToBuyController(ShoppingListCheckOffService) {
        var showTB = this;
        console.log(showTB);
        showTB.items = ShoppingListCheckOffService.showItems();
        showTB.buy = function (index) {
          ShoppingListCheckOffService.addBuy(index);
        };
    }

    AlreadyBoughtController.$inject = ["ShoppingListCheckOffService"]; // Protection from Injection
    function AlreadyBoughtController(ShoppingListCheckOffService) {
        var already = this;
        console.log(already);
        already.bought = ShoppingListCheckOffService.showBItems();
    }

    //Service Functionality
    function ShoppingListCheckOffService() {
      var service = this;
      console.log(service);
      var items = [{ name: "cookies", quantity: 10 }, { name: "biscuits", quantity: 10 },
{ name: "lollipops", quantity: 10 }, { name: "Sliced Oranges", quantity: 10 }, { name: "Pen", quantity: 10 }];
      var bought = [];
      service.showItems = function () {
        return items;
      };
      service.showBItems = function () {
        return bought;
      };
      service.addBuy = function (itemIndex) {
        bought.push(items[itemIndex]);
        items.splice(itemIndex, 1);
      }
    }
})();
