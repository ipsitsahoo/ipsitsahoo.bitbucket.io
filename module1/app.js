(function(){
  'use strict';

  angular.module('LunchCheck', [])
  .controller('LunchCheckController', LCont);
  LCont.inject = ['$scope']; //Protection from Injection

  function LCont($scope) {
    $scope.list = "";
    $scope.message = "";
    $scope.color = "white";
    $scope.border = "white";
    $scope.lcheck = function () {
      var list = $scope.list.trim().split(',');
      var c = 0;
      for (var i = 0;i <list.length; i++) {
        if(list[i].trim().length !== 0) {
          c++;
        }
      }
      if (c == 0) {
        $scope.message = "Please enter data first";
        $scope.color = "red";
        $scope.border = "red";
      }
      else if(c > 0 && c <=3) {
        $scope.color = "green";
        $scope.border = "green";
        $scope.message = "Enjoy!";
      } else if(c > 3) {
        $scope.color = "green";
        $scope.border = "green";
        $scope.message = "Too much!";
      }
    }
  }
})();
